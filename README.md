Project QTerm

version: 1.1.1
author: Dmitriy Loshakov

This is a basic terminal emulator app using LXDE's QTermWidget for the back-end and a QPushButton-powered on-screen keyboard (for touchscreens).

This project was put together primarily for the Raspberry Pi, with the official 7" touchscreen (800x480 resolution, DSI interface).

It is meant to be my first step into embedded development with Qt. I'm polishing things as I go, and I will have more projects to come.

Features:
- Full QWERTY keyboard with symbols and backspace functionality (made from scratch with QPushButtons), plus working Ctrl and Tab.
- Only requires one external library outside of Qt itself (QTermWidget).
- Don't need a physical keyboard to use your Pi w/ Touchscreen as a straight terminal.

Limitations:
- Current keyboard implementation is a bit slow to use effectively and is missing some functionality.

See TODO.md for features in progress.

Revision history:

= v1.0 =
- Implemented a 'fake' terminal using QProcess and a QTextBrowser widget
- Implemented a basic on-screen keyboard using QPushButton widgets
- Debug functionality to see what QProcess and it's children (the 'commands') are doing

= v1.1 =
- Implemented a relatively feature-rich terminal using LXDE's QTermWidget (see https://github.com/lxde/qtermwidget for more details).
- Improved keyboard functionality with inclusion of Tab, Ctrl, and the top-row keys (numbers, etc), as well as a slight layout adjustment.

= v1.1.1 =
- Implemented keyboard show/hide functionality
- Implemented exiting the app via an 'exit' command passed to the terminal
- Added Caps-Lock
