To-do:

- Separate out the keyboard code from the application
- Make the keyboard modular and customizable (themes, sizing, etc)
- Implement an interface for some of QTermWidget's customization (theming, font type/size, history size, etc)

I'll update this as I go.
