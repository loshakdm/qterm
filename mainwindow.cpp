#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    createActions();
    setupMenus();

    populateKeyMaps();
    hookupKeyboard();

    // Quick init of kb state flags
    shiftHeld = false;
    capsHeld = false;
    ctrlHeld = false;

    initTerminal();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initTerminal()
{
    termWidget = new QTermWidget(ui->termContainer);
    termWidget->resize(ui->termContainer->size());

    /** TODO: Figure out why setEnvironment doesn't work. According to how it is used internally
     * in qtermwidget, there is nothing to suggest why this doesn't work.

    QStringList env;
    env << "TERM=\"linux\"";

    termWidget->setEnvironment(env);

    **/

    termWidget->setScrollBarPosition(QTermWidget::ScrollBarRight);

    // Need to pass the full path since our environment is not quite standard
    termWidget->setColorScheme("/usr/local/qt5pi/share/qtermwidget5/color-schemes/GreenOnBlack.colorscheme");

    termWidget->startShellProgram();

    // Temporary fix for setEnvironment not working
    termWidget->sendText("export TERM=\"linux\"\n");
    termWidget->sendText("clear\n");

    termWidget->setFocusPolicy(Qt::StrongFocus);

    // For exiting the app if the user passes an 'exit' command
    connect(termWidget, SIGNAL(finished()), this, SLOT(Exit()));
}

void MainWindow::createActions()
{
    // To switch views between the terminal and QProcess debug info
    switchAction = new QAction(tr("Switch to Debug"), this);
    connect(switchAction, SIGNAL(triggered()), this, SLOT(viewSwitch()));

    fileMenuActions << switchAction;

    // To show/hide the keyboard
    kbShowAction = new QAction(tr("Hide Keyboard"), this);
    connect(kbShowAction, SIGNAL(triggered()), this, SLOT(showHideKeyboard()));

    fileMenuActions << kbShowAction;

    // To exit the program
    exitAction = new QAction(tr("Exit"), this);
    connect(exitAction, SIGNAL(triggered()), this, SLOT(Exit()));

    fileMenuActions << exitAction;
}

void MainWindow::setupMenus()
{
    fileMenu = new QMenu(tr("File"));
    fileMenu->addActions(fileMenuActions);

    ui->menuBar->addMenu(fileMenu);
}

void MainWindow::hookupKeyboard()
{
    QHashIterator<QPushButton*, QPair<QChar, QChar> > keyMapIter(keyMap);

    while (keyMapIter.hasNext()) {
        keyMapIter.next();

        connect(keyMapIter.key(), SIGNAL(clicked()), this, SLOT(keyPressed()));
        keyMapIter.key()->setFocusPolicy(Qt::NoFocus);
    }
}

void MainWindow::populateKeyMaps()
{
    keyMap.insert(ui->btnQuoteLeft, qMakePair('`', '~'));
    keyMap.insert(ui->btn0, qMakePair('0', ')'));
    keyMap.insert(ui->btn1, qMakePair('1', '!'));
    keyMap.insert(ui->btn2, qMakePair('2', '@'));
    keyMap.insert(ui->btn3, qMakePair('3', '#'));
    keyMap.insert(ui->btn4, qMakePair('4', '$'));
    keyMap.insert(ui->btn5, qMakePair('5', '%'));
    keyMap.insert(ui->btn6, qMakePair('6', '^'));
    keyMap.insert(ui->btn7, qMakePair('7', QChar(38)));
    keyMap.insert(ui->btn8, qMakePair('8', '*'));
    keyMap.insert(ui->btn9, qMakePair('9', '('));
    keyMap.insert(ui->btnDash, qMakePair('-', '_'));
    keyMap.insert(ui->btnEquals, qMakePair('=', '+'));
    keyMap.insert(ui->btnQ, qMakePair('q', 'Q'));
    keyMap.insert(ui->btnW, qMakePair('w', 'W'));
    keyMap.insert(ui->btnE, qMakePair('e', 'E'));
    keyMap.insert(ui->btnR, qMakePair('r', 'R'));
    keyMap.insert(ui->btnT, qMakePair('t', 'T'));
    keyMap.insert(ui->btnY, qMakePair('y', 'Y'));
    keyMap.insert(ui->btnU, qMakePair('u', 'U'));
    keyMap.insert(ui->btnI, qMakePair('i', 'I'));
    keyMap.insert(ui->btnO, qMakePair('o', 'O'));
    keyMap.insert(ui->btnP, qMakePair('p', 'P'));
    keyMap.insert(ui->btnA, qMakePair('a', 'A'));
    keyMap.insert(ui->btnS, qMakePair('s', 'S'));
    keyMap.insert(ui->btnD, qMakePair('d', 'D'));
    keyMap.insert(ui->btnF, qMakePair('f', 'F'));
    keyMap.insert(ui->btnG, qMakePair('g', 'G'));
    keyMap.insert(ui->btnH, qMakePair('h', 'H'));
    keyMap.insert(ui->btnJ, qMakePair('j', 'J'));
    keyMap.insert(ui->btnK, qMakePair('k', 'K'));
    keyMap.insert(ui->btnL, qMakePair('l', 'L'));
    keyMap.insert(ui->btnZ, qMakePair('z', 'Z'));
    keyMap.insert(ui->btnX, qMakePair('x', 'X'));
    keyMap.insert(ui->btnC, qMakePair('c', 'C'));
    keyMap.insert(ui->btnV, qMakePair('v', 'V'));
    keyMap.insert(ui->btnB, qMakePair('b', 'B'));
    keyMap.insert(ui->btnN, qMakePair('n', 'N'));
    keyMap.insert(ui->btnM, qMakePair('m', 'M'));
    keyMap.insert(ui->btnLeftBracket, qMakePair('[', '{'));
    keyMap.insert(ui->btnRightBracket, qMakePair(']', '}'));
    keyMap.insert(ui->btnBackSlash, qMakePair('\\', '|'));
    keyMap.insert(ui->btnColon, qMakePair(';', ':'));
    keyMap.insert(ui->btnApostrophe, qMakePair('\'', '"'));
    keyMap.insert(ui->btnComma, qMakePair(',', '<'));
    keyMap.insert(ui->btnPeriod, qMakePair('.', '>'));
    keyMap.insert(ui->btnForwardSlash, qMakePair('/', '?'));

    keyMap.insert(ui->btnTab, qMakePair(QChar(9), QChar(9)));
    keyMap.insert(ui->btnBackspace, qMakePair(QChar(8), QChar(8)));
    keyMap.insert(ui->btnShift, qMakePair(QChar(0), QChar(0))); // QChar(0)'s are just place holders, these values don't get used
    keyMap.insert(ui->btnEnter, qMakePair(QChar(0), QChar(0))); // We could technically use something like ASCII LF here but we don't have to
    keyMap.insert(ui->btnCtrl, qMakePair(QChar(0), QChar(0)));
    keyMap.insert(ui->btnSpace, qMakePair(QChar(32), QChar(32)));
    keyMap.insert(ui->btnCaps, qMakePair(QChar(0), QChar(0)));

    shiftExcludes << ui->btnTab << ui->btnBackspace << ui->btnShift << ui->btnEnter << ui->btnCtrl << ui->btnSpace << ui->btnCaps;
}

void MainWindow::viewSwitch() /// TODO: Deprecate or repurpose?
{
    QString whichPage = switchAction->text();

    if (whichPage.compare("Switch to Debug") == 0) {
        ui->viewWidget->setCurrentIndex(1);
        switchAction->setText("Switch to Terminal");
    } else {
        ui->viewWidget->setCurrentIndex(0);
        switchAction->setText("Switch to Debug");
    }
}

void MainWindow::showHideKeyboard()
{
    if (ui->frameKeyboard->isVisible()) {
        ui->frameKeyboard->setVisible(false);
        ui->termContainer->resize(ui->viewWidget->width() - 20, ui->viewWidget->height() - 10);
        termWidget->resize(ui->termContainer->size()); /// There might be a sizepolicy feature for doing this automatically

        kbShowAction->setText("Show Keyboard");
    } else {
        ui->frameKeyboard->setVisible(true);
        ui->termContainer->resize(ui->viewWidget->width() - 20, ui->viewWidget->height() - (ui->frameKeyboard->height() + 10));
        termWidget->resize(ui->termContainer->size());

        kbShowAction->setText("Hide Keyboard");
    }
}

void MainWindow::Exit() /// TODO: Put all clean-up here (heap alloc, etc)
{
    // termWidget->finished() can trigger this slot, so check the sender to prevent a redundant call to close()
    if (sender() != termWidget) {
        termWidget->close();
    }

    exit(0);
}

void MainWindow::keyPressed()
{
    /// TODO: Implement Shift/Ctrl/Caps highlighting

    // These are the 'special' keys, define their function first
    if (sender() == ui->btnShift) {
        shiftHeld = !shiftHeld;
        showSymbols();

    } else if (sender() == ui->btnCaps) {
        capsHeld = !capsHeld;
        showSymbols();

    } else if (sender() == ui->btnCtrl) {
        ctrlHeld = !ctrlHeld;

    } else if (sender() == ui->btnBackspace) {
        termWidget->sendText(QChar(8));

    } else if (sender() == ui->btnEnter) {
        termWidget->sendText("\n");

    } else if (sender() == ui->btnSpace) {
        termWidget->sendText(" ");

    } else if (sender() == ui->btnTab) {
        termWidget->sendText(QChar(9));

    } else {
        QChar typedChar;

        if (shiftHeld) {
            shiftHeld = !shiftHeld;
            showSymbols();

            typedChar = keyMap.value(qobject_cast<QPushButton*>(sender())).second;

        } else if (capsHeld) {
            typedChar = keyMap.value(qobject_cast<QPushButton*>(sender())).second;

        } else {
            typedChar = keyMap.value(qobject_cast<QPushButton*>(sender())).first;
        }

        if (ctrlHeld) {
            ctrlHeld = !ctrlHeld;

            if (typedChar.isLetter()) {
                typedChar = typedChar.toUpper();
            }

            typedChar = QChar(typedChar.toLatin1() - 64);
        }

        termWidget->sendText(typedChar);
    }
}

void MainWindow::showSymbols()
{
    QHashIterator<QPushButton*, QPair<QChar, QChar> > keyMapIter(keyMap);
    QString keyText;

    if (shiftHeld || capsHeld) {
        while (keyMapIter.hasNext()) {
            keyMapIter.next();

            if (!shiftExcludes.contains(keyMapIter.key())) {
                keyText = keyMapIter.value().second;
                keyMapIter.key()->setText(keyText);
            }
        }
    } else {
        while (keyMapIter.hasNext()) {
            keyMapIter.next();

            if (!shiftExcludes.contains(keyMapIter.key())) {
                keyText = keyMapIter.value().first;
                keyMapIter.key()->setText(keyText);
            }
        }
    }
}
