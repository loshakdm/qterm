#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <qtermwidget5/qtermwidget.h>

namespace Ui {
class MainWindow;
}

class QPushButton;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:

private slots:
    void viewSwitch();
    void showHideKeyboard();
    void Exit();

    void keyPressed();

private:
    Ui::MainWindow *ui;
    QTermWidget *termWidget;
    QMenu *fileMenu;
    QList<QAction *> fileMenuActions;
    QHash<QPushButton*, QPair<QChar, QChar> > keyMap;
    QList<QPushButton*> shiftExcludes;

    QAction *switchAction;
    QAction *exitAction;
    QAction *kbShowAction;

    bool shiftHeld;
    bool capsHeld;
    bool ctrlHeld;

    void initTerminal();
    void createActions();
    void setupMenus();
    void hookupKeyboard();
    void populateKeyMaps();
    void showSymbols();
};

#endif // MAINWINDOW_H
